const { default: axios } = require("axios");

const API = axios.create({
    baseURL: 'http://localhost:5000',
    withCredentials: true
});

export default API;
