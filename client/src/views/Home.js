import React, { useEffect, useState } from "react";
import API from "../API";
import MultiLingualContent from "../components/MultiLingualContent";


function Home() {

    // states for truelink and shortLinkId
    const [trueLink, setTrueLink] = useState("");
    const [shortLinkId, setShortLinkId] = useState("");

    const [link, setLink] = useState(null);

    // states for success and error messages
    const [successMessage, setSuccessMessage] = useState(false);
    const [errorMessage, setErrorMessage] = useState(false);
    const [errorMax5, setErrorMax5] = useState(false);


    const [connected, setConnected] = useState(false);
    const [userId, setUserId] = useState(null);

    // func hundlechanges inputs 
    const handleChangeTrueLink = (event) => { setTrueLink(event.target.value); }
    const handleChangeShortLinkId = (event) => { setShortLinkId(event.target.value); }


    useEffect(() => {
        API.get("api/user/connected")
            .then(res => {
                setUserId(res.data._id);
                setConnected(true);
            }
                , err => {
                    setConnected(false);
                }
            );
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        // if connected then get user info
        if (!connected)
            window.location.href = "/login";

        if (trueLink === "" || shortLinkId === "") {
            setErrorMessage(true);
            setSuccessMessage(false);
        }

        await API.post("/api/link", {
            "trueLink": trueLink,
            "shortLinkId": shortLinkId,
            "userId": userId
        }).then(res => {
            console.log(res);
            setSuccessMessage(true);
            setErrorMessage(false);
            setLink("http://localhost:3000/" + res.data.link.shortLinkId);
            setShortLinkId("");
            setTrueLink("");
        }, err => {
            // if error is max 5 then show error message
            if (err.response.status === 400 && err.response.data.error === "limit5") {
                setErrorMax5(true);
                setErrorMessage(false);
                setSuccessMessage(false);
            }
            else {
                console.log(err);
                setErrorMessage(true);
                setSuccessMessage(false);
            }
        }
        );
    }


    return (
        <div className="d-flex justify-content-center m-3" >
            <div className=" px-5 shadow border w-75 p-3">
                <div className="m-5" >
                    <h1 style={{ color: "blue", fontSize: "50px", fontWeight: "bold" }}>
                        <MultiLingualContent contentID="titleHomebody" />
                    </h1>
                    <p>
                        <MultiLingualContent contentID="descHomeBody" />
                    </p>
                </div>
                <div className="d-flex justify-content-center">

                    <div className="w-50" >
                        {
                            successMessage &&
                            <div className="alert alert-success" role="alert">
                                <MultiLingualContent contentID="successMessage" />
                                <span class="" id="basic-addon1">Short link Id  :</span>
                                <span class="input-group-text" id="basic-addon1">{link}</span>
                            </div>
                        }
                        {
                            errorMessage &&
                            <div className="alert alert-danger" role="alert">
                                <MultiLingualContent contentID="errorMessage" />
                            </div>
                        }
                        {
                            errorMax5 &&
                            <div className="alert alert-danger" role="alert">
                                <MultiLingualContent contentID="errorMax5" />
                            </div>
                        }
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1">Link :</span>
                            <input type="text" class="form-control" value={trueLink} placeholder="https://exemple.com/upierzjhgpijer/reohipjpeoirjhpoijeoj" aria-label="Username" aria-describedby="basic-addon1" onChange={handleChangeTrueLink} />
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1">Short link Id  :</span>
                            <input type="text" class="form-control" value={shortLinkId} placeholder="test1" aria-label="shortLinkId" aria-describedby="basic-addon1" onChange={handleChangeShortLinkId} />
                        </div>
                        <button type="button" class="btn btn-primary" onClick={handleSubmit}><MultiLingualContent contentID="shortenLink" /></button>
                    </div>
                </div>
            </div>
        </div >
    );
}


export default Home;