import React, { useState } from "react";
import API from "../API";
import MultiLingualContent from "../components/MultiLingualContent";


function Login() {

    // email and password are set to empty strings
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const [failed, setFailed] = useState(false);

    // function to handle the change of the email input
    const handleEmailChange = (event) => {
        setEmail(event.target.value);
    }

    // function to handle the change of the password input
    const handlePasswordChange = (event) => {
        setPassword(event.target.value);
    }

    function handleSubmit(event) {
        API.post("/auth/login", {
            "email": email,
            "password": password
        }, {
            credentials: 'include',
        }
        ).then(response => {
            localStorage.setItem("userId", response.data.user || "");
            window.location.href = "/";
        }, (error) => {
            setFailed(true);
        }
        );
    }

    return (
        <div className="d-flex justify-content-center m-3">
            <div className=" px-5 shadow border w-75 p-3">
                <div className="m-5" >
                    <h1 style={{ color: "blue", fontSize: "50px", fontWeight: "bold" }}>
                        <MultiLingualContent contentID="login" />
                    </h1>
                </div>
                <div className="d-flex justify-content-center">
                    <div className="w-50" >
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1">Email :</span>
                            <input type="email" class="form-control" aria-label="email" aria-describedby="basic-addon1" onChange={handleEmailChange} />
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1">
                                <MultiLingualContent contentID="password" />
                            </span>
                            <input type="password" class="form-control" placeholder="password" aria-label="password" aria-describedby="basic-addon1" onChange={handlePasswordChange} />
                        </div>
                        <button type="button" class="btn btn-primary" onClick={ handleSubmit }><MultiLingualContent contentID="login" /></button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Login;