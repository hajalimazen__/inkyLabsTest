import React, { useEffect, useState } from "react";
import API from "../API";
import MultiLingualContent from "../components/MultiLingualContent";


function Dashboard() {
    const [links, setLinks] = useState([]);

    const deleteLink = async (id) => {
        await API.delete("/api/link/" + id)
            .then(res => {
                setLinks(links.filter(link => link._id !== id));
            }, err => {
                console.log(err);
            });

    }


    useEffect(() => {
        console.log("Dashboard.js");

        const userId = localStorage.getItem("userId");

        API.get("api/link/user/" + userId).then(res => {
            console.log(res);
            setLinks(res.data);
        }, err => {
            console.log(err);
            // if not connected redirect to login
            window.location.href = "/login";
        });
    }, []);

    return (
        <div className="d-flex justify-content-center m-3">
            <div className=" px-5 shadow border w-100 p-3">
                <div className="m-5" >
                    <h1 style={{ color: "blue", fontSize: "50px", fontWeight: "bold" }}>
                        <MultiLingualContent contentID="dashboard" />
                    </h1>
                </div>
                <div className="d-flex justify-content-center">
                    <div className="w-75" >
                        {
                            links.map((link) => {
                                return (
                                    <div className="d-flex justify-content-center" id={link.shortLinkId} key={link.shortLinkId}>
                                        <div className="w-100" >
                                            <div class="input-group mb-3">
                                                <span class="input-group-text" id="basic-addon1">{link.shortLinkId}</span>
                                                <input type="text" class="form-control" defaultValue={link.trueLink} aria-label="username" aria-describedby="basic-addon1" disabled={true} />
                                                <span class="btn btn-danger" id="basic-addon1" onClick={() => { deleteLink(link.shortLinkId); document.getElementById(link.shortLinkId).remove(); }}>
                                                    <MultiLingualContent contentID="delete" />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                );
                            }
                            )
                        }
                    </div>
                </div>
            </div>
        </div>
    );
}


export default Dashboard;