import React, { useEffect, useState } from "react";
import API from "../API";
import MultiLingualContent from "../components/MultiLingualContent";
import { useParams } from "react-router-dom";


function Redirect() {

    // get the shortLinkId from path using useparams
    const { shortLinkId } = useParams();
    const [RedirectError, setRedirectError] = useState(false);
    const [trueLink, setTrueLink] = useState(null);

    useEffect(() => {
        API.get("redirect/" + shortLinkId)
            .then(res => {
                console.log(res.data);
                setTrueLink(res.data);
                setTimeout(() => {
                    window.location.href = res.data;
                }, 4000);

            }, err => {
                console.log(err);
                setRedirectError(true);
            });
    }, []);
    return (
        <div className="d-flex justify-content-center m-3">
            <div className=" px-5 shadow border w-75 p-3">
                <div className="m-5" >
                    <h2 style={{ color: "blue", fontWeight: "bold" }}>
                        {
                            window.location.href

                        }
                    </h2>
                </div>
                <div className="d-flex justify-content-center">
                    <div className="w-50" >
                        {
                            RedirectError ? (
                                <div>
                                    {/* alert */}
                                    <div className="alert alert-danger" role="alert">
                                        <MultiLingualContent contentID="redirectError" />
                                    </div>
                                    <div className="alert alert-danger" role="alert">
                                        <MultiLingualContent contentID="redirectErrorDesc" />
                                    </div>
                                    
                                </div>
                            ) : (
                                <div>
                                    <div className="d-flex justify-content-center">
                                        <MultiLingualContent contentID="redirecting" /> :
                                    </div>
                                    <div className="d-flex justify-content-center">
                                        <a href={trueLink}>{trueLink}</a>
                                    </div>
                                </div>
                            )
                        }
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Redirect;