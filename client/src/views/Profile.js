import React, { useEffect, useState } from "react";
import API from "../API";
import MultiLingualContent from "../components/MultiLingualContent";


function Profile() {

    const [user, setUser] = useState(null);
    const [modify, setModify] = useState(true);


    useEffect(() => {
        console.log("Dashboard.js");
        API.get("api/user/connected").then(res => {
            console.log(res);
            setUser(res.data);
        }, err => {
            console.log(err);
            // if forbidden then redirect to login else display error
            if (err.response.status === 403) {
                window.location.href = "/login";
            } else {
                alert(err.response.data.message);
            }
        });
    }, []);

    return (
        <div className="d-flex justify-content-center m-3">
            <div className=" px-5 shadow border w-75 p-3">
                <div className="m-5" >
                    <h1 style={{ color: "blue", fontSize: "50px", fontWeight: "bold" }}>
                        <MultiLingualContent contentID="profile" />
                    </h1>
                </div>
                <div className="d-flex justify-content-center">
                    <div className="w-50" >
                        {
                            user &&
                            <div>
                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="basic-addon1">Email :</span>
                                    <input type="email" class="form-control" defaultValue={user.email} aria-label="email" aria-describedby="basic-addon1" disabled={modify} />
                                </div>
                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="basic-addon1">
                                        <MultiLingualContent contentID="nom" /> :
                                    </span>
                                    <input type="text" class="form-control" defaultValue={user.nom} aria-label="nom" aria-describedby="basic-addon1" disabled={modify} />
                                </div>
                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="basic-addon1">
                                        <MultiLingualContent contentID="prenom" /> :
                                    </span>
                                    <input type="text" class="form-control" defaultValue={user.prenom} aria-label="prenom" aria-describedby="basic-addon1" disabled={modify} />
                                </div>
                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="basic-addon1">
                                        <MultiLingualContent contentID="numTel" /> :
                                    </span>
                                    <input type="text" class="form-control" defaultValue={user.numTel} aria-label="numTel" aria-describedby="basic-addon1" disabled={modify} />
                                </div>
                                <div class="input-group mb-3">
                                    <span  id="basic-addon1">Created At :</span>
                                    <span  id="basic-addon1">{user.createdAt}</span>
                                </div>
                            </div>

                        }

                    </div>
                </div>
            </div>
        </div>
    );
}

export default Profile;