import React, { useState } from "react";
import API from "../API";
import MultiLingualContent from "../components/MultiLingualContent";


function Register() {

    // email and password are set to empty strings
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [nom, setNom] = useState("");
    const [prenom, setPrenom] = useState("");
    const [sexe, setSexe] = useState("");
    const [numTel, setNumTel] = useState("");

    const [failed, setFailed] = useState(false);

    // function to handle the change of the  inputs
    const handleEmailChange = (event) => { setEmail(event.target.value); }
    const handlePasswordChange = (event) => { setPassword(event.target.value); }
    const handleNomChange = (event) => { setNom(event.target.value); }
    const handlePrenomChange = (event) => { setPrenom(event.target.value); }
    const handleSexeChange = (event) => { setSexe(event.target.value); }
    const handleNumTelChange = (event) => { setNumTel(event.target.value); }



    function handleSubmit(event) {
        API.post("/auth/register", {
            "email": email,
            "password": password,
            "nom": nom,
            "prenom": prenom,
            "sexe": sexe,
            "numTel": numTel
        }, {
            credentials: 'include',
        }
        ).then(response => {
            window.location.href = "/login";
        }, (error) => {
            setFailed(true);
        }
        );
    }

    return (
        <div className="d-flex justify-content-center m-3">
            <div className=" px-5 shadow border w-75 p-3">
                <div className="m-5" >
                    <h1 style={{ color: "blue", fontSize: "50px", fontWeight: "bold" }}>
                        <MultiLingualContent contentID="register" />
                    </h1>
                </div>
                <div className="d-flex justify-content-center">
                    <div className="w-50" >
                    {
                        failed && <div className="alert alert-danger" role="alert">
                            <MultiLingualContent contentID="registerFailed" />
                        </div>
                    }
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1">Email :</span>
                            <input type="email" class="form-control" aria-label="email" aria-describedby="basic-addon1" onChange={handleEmailChange} />
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1">
                                <MultiLingualContent contentID="password" />
                            </span>
                            <input type="password" class="form-control" placeholder="password" aria-label="password" aria-describedby="basic-addon1" onChange={handlePasswordChange} />
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1">
                                <MultiLingualContent contentID="nom" /> :
                            </span>
                            <input type="text" class="form-control" placeholder="nom" aria-label="password" aria-describedby="basic-addon1" onChange={handleNomChange} />
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1">
                                <MultiLingualContent contentID="prenom" /> :
                            </span>
                            <input type="text" class="form-control" placeholder="prenom" aria-label="password" aria-describedby="basic-addon1" onChange={handlePrenomChange} />
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1">
                                <MultiLingualContent contentID="numTel" /> :
                            </span>
                            <input type="text" class="form-control" placeholder="numero tel" aria-label="password" aria-describedby="basic-addon1" onChange={handleNumTelChange} />
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1">
                                <MultiLingualContent contentID="gender" /> :
                            </span>
                            <input type="text" class="form-control" placeholder="sexe" aria-label="password" aria-describedby="basic-addon1" onChange={handleSexeChange} />
                        </div>
                        <button type="button" class="btn btn-primary" onClick={handleSubmit}><MultiLingualContent contentID="register" /></button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Register;