import { useEffect, useState } from "react";
import { BrowserRouter, Route, Routes, Link } from "react-router-dom";
import API from "./API";
import './App.css';
import Header from './components/Header';
import Redirect from "./views/Redirect";
import { LanguageContext } from "./Context";
import Dashboard from "./views/Dashboard";
import Home from './views/Home';
import Login from "./views/Login";
import Profile from "./views/Profile";
import Register from "./views/Register";



function App() {

  useEffect(() => {
    console.log("App.js");
    // check if language is set in localStorage
    const L = localStorage.getItem("language");
    // check if language is supported english or french
    if (L === "english" || L === "french") {
      setLanguage(L);
    } else {
      setLanguage("english");
      localStorage.setItem("language", "english");
    }
  }, []);


  const [language, setLanguage] = useState("english");

  function toggleLanguage() {
    setLanguage((language) => (language === "english" ? "french" : "english"));
    localStorage.setItem("language", language==="english" ? "french" : "english");
  }


  return (
    <div className='App'>
      <div className="Body">
        <LanguageContext.Provider value={{ language, toggleLanguage }}>
          <BrowserRouter>
            <div>
              <Header />
              <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/login" element={<Login />} />
                <Route path="/register" element={<Register />} />
                <Route path="/dashboard" element={<Dashboard />} />
                <Route path="/profile" element={<Profile />} />
                {/* route that execute a function not a component */}
                <Route path="/:shortLinkId" element={<Redirect />} />


              </Routes>
            </div>
          </BrowserRouter>
        </LanguageContext.Provider>
      </div>
    </div>
  );
}

export default App;
