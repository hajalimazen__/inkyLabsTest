import React, { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import API from "../API";
import { LanguageContext } from "../Context";
import MultiLingualContent from "./MultiLingualContent";

const NavBar = () => {

  // state for auth check
  const [isAuthenticated, setIsAuthenticated] = useState(false);

  // get the language from context
  const { language, toggleLanguage } = useContext(LanguageContext);


  useEffect(() => {
    console.log("NavBar.js");
    API.get("/api/authchecker")
      .then(res => {
        // if res.data is true, user is authenticated
        setIsAuthenticated(res.data);
      }, err => {
        console.log(err);
      }
      );
  }, []);

  // function to logout
  const logout = async () => {
    await API.post("/auth/logout")
      .then(res => {
        setIsAuthenticated(false);
        // redirect to home
        window.location.href = "/";
      }
      );
  }



  return (

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <div class="container justify-content-between">
        <a class="navbar-brand" href="/">
          <svg id="InkyLab" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 800 400" className="w-75" >
            <g id="InkyLabLogo"><path fill="#FF3333" d="M458.8,222.3h33.4v6.4h-41v-56.8h7.6V222.3z M529.2,171.9l24.4,56.8h-8.1l-7.1-16.7h-25.1l-7,16.7h-8
 l24.5-56.8C522.8,171.9,529.2,171.9,529.2,171.9z M536.8,206.5L526,180.2l-11.1,26.3H536.8z M607.6,214.1c0,2.2-0.5,4.2-1.4,6
 c-0.9,1.8-2.2,3.4-3.8,4.6c-1.6,1.3-3.5,2.3-5.6,3c-2.1,0.7-4.4,1-6.8,1h-28.4v-56.8h29.8c2.1,0,4,0.4,5.6,1.3
 c1.7,0.9,3.1,2,4.3,3.3c1.2,1.4,2.1,2.9,2.7,4.6c0.6,1.7,1,3.5,1,5.2c0,2.8-0.7,5.4-2.2,7.8c-1.5,2.4-3.6,4.2-6.4,5.4
 c3.4,1,6.1,2.7,8.1,5.3C606.6,207.5,607.6,210.5,607.6,214.1z M569.2,197.1h19c1.3,0,2.6-0.3,3.7-0.8c1.1-0.5,2.1-1.2,2.9-2
 c0.8-0.9,1.5-1.9,1.9-3c0.5-1.1,0.7-2.4,0.7-3.6c0-1.3-0.2-2.6-0.7-3.8s-1.1-2.2-1.8-3c-0.8-0.9-1.7-1.5-2.8-2
 c-1.1-0.5-2.3-0.7-3.5-0.7h-19.4V197.1L569.2,197.1z M599.9,212.8c0-1.3-0.2-2.5-0.7-3.7s-1.1-2.3-1.9-3.2
 c-0.8-0.9-1.8-1.6-2.9-2.2c-1.1-0.5-2.4-0.8-3.7-0.8h-21.4v19.6h20.8c1.4,0,2.7-0.3,3.9-0.8c1.2-0.5,2.2-1.2,3.1-2.1
 c0.9-0.9,1.6-1.9,2.1-3.1S599.9,214.1,599.9,212.8z"></path><path d="M264.7,171.9h3v56.8h-3V171.9z M328,224.3l-44-52.4h-2.2v56.9h2.9v-51.6l43.3,51.5h2.9v-56.8H328V224.3z M388.1,171.9h-3.6
 l-36.6,35.8v-35.8H345v56.8v0.1h2.9v-17.6l13-12.7l24.9,30.2h3.5l-26.4-31.9L388.1,171.9z M439,171.9l-22.2,32.3l-22-32.3h-3.4
 l23.9,34.9v21.9h3v-21.8l23.9-35H439z M250.9,171.9v56.3c0,0.2,0.1,0.4,0,0.5c-0.1,0.2-0.3,0.4-0.6,0.5l-56.4,56.5
 c-0.2,0.1-0.4,0.2-0.6,0.2c-0.1,0-0.2,0-0.3-0.1c-0.3-0.1-0.5-0.4-0.5-0.7v-56.7v-0.1v-56.5c0-0.2-0.1-0.4,0-0.5
 c0.1-0.2,0.2-0.4,0.5-0.5l28.1-28.1l0.1-0.1l28.3-28.3c0.2-0.2,0.6-0.3,0.9-0.2c0.3,0.1,0.5,0.4,0.5,0.7v56.6
 C250.9,171.6,251,171.7,250.9,171.9z M195.3,227.5H248l-26.3-26.3L195.3,227.5z M248.1,172.4h-52.9l26.5,26.4L248.1,172.4z
 M194.1,226.5l26.5-26.5l-26.5-26.4V226.5z M249.3,173.5l-26.4,26.4l26.4,26.5V173.5z M249.3,116.7l-26.4,26.4l26.4,26.5V116.7z
 M195.2,170.8h52.9l-26.4-26.5L195.2,170.8z M248.2,229.2h-54.1v54.1L248.2,229.2z"></path>
            </g>
          </svg>
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="/">
                <MultiLingualContent contentID="Home" />
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link mx-3" href="/dashboard">
                <MultiLingualContent contentID="dashboard" />
              </a>
            </li>
          </ul>
        </div>

        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent2" aria-controls="navbarSupportedContent2" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent2">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item mx-2">
              {/* button for toggling language */}
              <button class="btn btn-outline-primary" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onClick={toggleLanguage}>
                {
                  language === "english" ? "passer au français" : "switch to English"
                }
              </button>

            </li>
            <li class="nav-item">
              {
                isAuthenticated ? (
                  <>
                    <a class="nav-link active" aria-current="page" href="/profile">
                      <MultiLingualContent contentID="profile" />
                    </a>
                    <button type="button" class="btn btn-light" onClick={logout}>Logout</button>
                  </>
                ) : (
                  <>
                    <a class="nav-link active" aria-current="page" href="/register">
                      <MultiLingualContent contentID="register" />
                    </a>
                    <a class="nav-link mx-3" href="/login">
                      <MultiLingualContent contentID="login" />
                    </a>
                  </>
                )
              }
            </li>
          </ul>
        </div>
      </div>
    </nav>

  );
};

export default NavBar;
