const jwt = require('jsonwebtoken');
const UserModel = require('../models/user.model');

module.exports.checkUser = (req, res, next) => {
    const token = req.cookies.jwt;
    if (!token) {
        return res.sendStatus(403);
    }
    try {
        jwt.verify(token, process.env.TOKEN_SECRET, async(err, decodedToken) => {
            if(err){
                res.locals.user = null ;
                res.cookies('jwt', '');
                return next();
            } else {
                let user = await UserModel.findById(decodedToken.id);
                res.locals.user = user ;
                return next();
            }
        });
    } catch {
        return res.sendStatus(403);
    }
}

module.exports.requireAuth = ( req,res, next) => {
    const token = req.cookies.jwt;
    if(token) {
        jwt.verify(token, process.env.TOKEN_SECRET, async(err, decodedToken) => {
            if(err){
                console.log(err);
            } else {
                // console.log(decodedToken.id);
                return next();
            }
        });
    } else {
        // console.log('No token');
        return res.sendStatus(403);
    }
};

// module.exports.checkAuthAdmindep