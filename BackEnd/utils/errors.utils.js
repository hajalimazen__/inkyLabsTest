module.exports.signUpErrors = (err) => {
    let errors = { email: '', password: '' }

    if (err.message.includes('email'))
        errors.email = "Email incorrect";

    if (err.code == 11000 && Object.keys(err.keyValue)[0].includes('email'))
        errors.email = "cet email est deja enregistre";
    return errors
}

module.exports.signInErrors = (err) => {
    let errors = { email: '', password: '' }

    if (err.message.includes('email'))
        errors.email = "Email inconnu";

    if (err.message.includes('password'))
        errors.password = "Password incorrect";

    return errors
}