const router = require('express').Router();
const linkController = require('../controllers/link.controller');

router.get('/', linkController.getAllLinks);
router.get('/:shortLinkId', linkController.getLinkByLinkId);
router.post('/', linkController.createLink);
router.get('/user/:userId', linkController.getAllLinksByUserId);
router.delete('/:shortLinkId', linkController.deleteLink);

module.exports = router;