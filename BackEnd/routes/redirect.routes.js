const Router = require('express').Router();
const redirectController = require('../controllers/redirect.controller');


Router.get('/:shortLinkId', redirectController.redirect);


module.exports = Router;