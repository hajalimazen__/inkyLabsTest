const UserModel = require('../models/user.model');
const jwt = require('jsonwebtoken');
const { signUpErrors, signInErrors } = require('../utils/errors.utils');

const maxAge = 3 * 24 * 60 * 60 * 1000;

const createToken = (id) => {
    return jwt.sign({ id }, process.env.TOKEN_SECRET, { expiresIn: maxAge })
};

module.exports.signUp = async (req, res) => {
    const { nom, prenom, ident, sexe, numTel, email, password , github , linkedIn} = req.body

    try {
        const user = await UserModel.create({ nom, prenom, sexe, numTel, email, password });
        return res
            .status(201)
            .json({ user: user });
    } catch (err) {
        console.log(err);
        const errors = signUpErrors(err);
        return res
            .status(400)
            .send({errors});
    }
}

module.exports.signIn = async (req, res) => {
    const { email, password } = req.body
    try {
        const user = await UserModel.login(email, password);
        const token = createToken(user._id);
        return res
            .cookie('jwt', token, { httpOnly: true, maxAge })
            .status(200)
            .json({ user: user._id });
    } catch (err) {
        const errors = signInErrors(err);
        res
            .status(400)
            .json({errors});
    }
}

module.exports.signOut = (req, res) => {
    return res
        .clearCookie("jwt")
        .status(200)
        .json({ message: "Déconnexion réussie. 😏" });
}
