const UserModel = require('../models/user.model');
const ObjectID = require('mongoose').Types.ObjectId;
const jwt = require('jsonwebtoken');

module.exports.getAllUsers = async (req, res) => {
    const users = await UserModel.find().select('-password');
    return res
        .status(200)
        .json(users);
};

module.exports.updateUser = async (req, res) => {
    try {
        UserModel.findOneAndUpdate({ _id: req.params.id }, { $set: req.body }, { new: true })
            .then((docs) => res.send(docs))
            .catch((err) => res.status(500).send({ message: err }));
    } catch (err) {
        return res
            .status(500)
            .json({ message: err });
    }
};

module.exports.deleteUser = async (req, res) => {
    if (!ObjectID.isValid(req.params.id))
        return res
            .status(400)
            .send("ID unknown", req.params.id);

    try {
        await UserModel.remove({ _id: req.params.id }).exec();
        return res
            .status(200)
            .json({ message: "Successfully deleted" });
    } catch (err) {
        return res
            .status(500)
            .send({ message: err });
    }
}

// module that returns the connected user
module.exports.getConnectedUser = (req, res) => {
    // token is in the cookies
    const token = req.cookies.jwt;
    if (!token) {
        return res.sendStatus(403);
    }
    try {
        jwt.verify(token, process.env.TOKEN_SECRET, async(err, decodedToken) => {
            if(err){
                res.locals.user = null ;
                res.cookies('jwt', '');
                return next();
            } else {
                console.log("decodedToken.id", decodedToken.id);
                let user = await UserModel.findById(decodedToken.id).select('-password');
                res.status(200).send(user);
            }
        });
    } catch {
        return res.sendStatus(404);
    }
}