const LogModel = require('../models/log.model');
const LinkModel = require('../models/link.model');
const { lookup } = require('geoip-lite');

const jwt = require('jsonwebtoken');


module.exports.redirect = async (req, res) => {
    const link = await LinkModel.findOne({ shortLinkId: req.params.shortLinkId });
    if (!link) {
        return res
            .status(404)
            .send({ message: "Link not found" });
    }

    let token = req.cookies.jwt;
    let userId = null;
    if (token) {
        jwt.verify(token, process.env.TOKEN_SECRET, async (err, decodedToken) => {
            if (err) {
                console.log(err);
            } else {
                console.log(decodedToken.id);
                userId = decodedToken.id;
            }
        }
        );
    }

    
    console.log(req.headers['user-agent'])
    const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    const geo = lookup(ip);
    // console.log(ip);
    // console.log(link);
    // console.log(userId);

    // the geo must be a object with the following properties: but it will be null because the ip is not valid ( local host )
    // {
    //     range: [3479298048, 3479300095],
    //         country: 'US',
    //             region: 'TX',
    //                 eu: '0',
    //                     timezone: 'America/Chicago',
    //                         city: 'San Antonio',
    //                             ll: [29.4969, -98.4032],
    //                                 metro: 641,
    //                                     area: 1000
    // }

    // create log
    await LogModel.create({
        userId : userId ? userId : null,
        shortLinkId: link.shortLinkId,
        trueLink: link.trueLink,
        ip: ip,
        userAgent: req.headers['user-agent'],
        country: geo ? geo.country : null,
    });

    // send the true link
    return res
        .status(201)
        .send(link.trueLink);

    // redirect to the true link
    // return res
    //     .status(301)
    //     .redirect(link.trueLink);
}
