const LinkModel = require('../models/link.model');
const ObjectID = require('mongoose').Types.ObjectId;


module.exports.getAllLinks = async (req, res) => {
    const links = await LinkModel.find().select('-_id -__v');
    return res
        .status(200)
        .json(links);
}


module.exports.getLinkByLinkId = (req, res) => {
    LinkModel.findOne({ shortLinkId: req.params.shortLinkId }, (err, docs) => {
        if (!err) res.send(docs);
        else console.log("shortLinkId unknown", err);
    }).select('-_id -__v');
}


module.exports.createLink = async (req, res) => {
    const { trueLink, shortLinkId, userId } = req.body;
    try {

        //count the number of links for the user and check if the user has reached the limit of 5
        const count = await LinkModel.countDocuments({ userId: userId });
        if (count >= 5) {
            return res
                .status(400)
                .json({ error: "limit5" });
        }

        //count the number of links 
        const countAll = await LinkModel.countDocuments();
        if (countAll >= 20) {
            // delete the oldest link
            const oldestLink = await LinkModel.findOne({}).sort({ createdAt: 1 });
            await LinkModel.remove({ _id: oldestLink._id });
        }


        const link = await LinkModel.create({ trueLink, shortLinkId, userId });
        return res
            .status(201)
            .json({ link: link });
    } catch (err) {
        console.log(err);
        return res
            .status(400)
            .send({ message: err });
    }
}


module.exports.getAllLinksByUserId = async (req, res) => {
    const links = await LinkModel.find({ userId: req.params.userId }).select('-_id -__v');
    return res
        .status(200)
        .json(links);
}


module.exports.deleteLink = async (req, res) => {
    try {
        await LinkModel.remove({ shortLinkId: req.params.shortLinkId }).exec();
        return res
            .status(200)
            .json({ message: "Successfully deleted" });
    } catch (err) {
        return res
            .status(500)
            .send({ message: err });
    }
}