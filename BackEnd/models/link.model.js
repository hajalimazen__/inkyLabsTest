const mongoose = require('mongoose');

const linkSchema = new mongoose.Schema(
  {
    trueLink: {
      type: String,
      required: true,
    },
    shortLinkId: {
      type: String,
      required: true,
      unique: true,
    },
    userId: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

const LinkModel = mongoose.model("link", linkSchema);

module.exports = LinkModel;