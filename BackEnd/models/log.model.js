const mongoose = require('mongoose');

const logSchema = new mongoose.Schema(
    {
        userId: {
            type: String,
        },
        shortLinkId: {
            type: String,
            required: true,
        },
        trueLink: {
            type: String,
            required: true,
        },
        ip: {
            type: String,
            required: true,
        },
        country: {
            type: String,
        },
        userAgent: {
            type: String,
            required: true,
        }
    },
    {
        timestamps: true,
    }
);

const LogModel = mongoose.model('log', logSchema);

module.exports = LogModel;