const express = require('express');
const cookieParser = require("cookie-parser");

const { requireAuth } = require('./middleware/auth.middleware');

const userRoutes = require('./routes/user.routes');
const authRoutes = require('./routes/auth.routes');
const linkRoutes = require('./routes/link.routes');
const redirectRoutes = require('./routes/redirect.routes');


require('dotenv').config({ path: './config/.env' });
// La configuración de la base de données mongoDB
require('./config/db');
const cors = require('cors');

const app = express();


const corsOptions = {
    origin: ["http://localhost:3000"],
    credentials: true
};

app.use(express.json());
app.use(cookieParser());
app.use(cors(corsOptions));

app.use('/api/user',requireAuth, userRoutes);
app.use('/auth', authRoutes);
app.use('/api/link/', requireAuth, linkRoutes);
app.use('/redirect/', redirectRoutes);

// route that returns boolean if user is authenticated based on middleware requireAuth
app.get('/api/authchecker', requireAuth, (req, res) => { res.send(true); })

// server
app.listen(process.env.PORT, () => {
    console.log('listening on port %d', process.env.PORT);
})